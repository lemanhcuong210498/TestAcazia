package com.lecuong.service;

import java.util.List;

import com.lecuong.entity.Product;

public interface ProductService {
	
	void sortProductByPrice(String searchCategoryTag);
	
	void sortProductByName(String searchCategoryTag);
	
}
