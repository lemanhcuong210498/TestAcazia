package com.lecuong.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import com.lecuong.controller.ProductController;
import com.lecuong.entity.Category;
import com.lecuong.entity.Product;
import com.lecuong.repository.ProductRepository;
import com.lecuong.repository.impl.ProductRepositoryImpl;
import com.lecuong.service.ProductService;
import com.lecuong.service.impl.ProductServiceImpl;

public class Main {

	public static void main(String[] args) {
		
		ProductRepository productRepository = new ProductRepositoryImpl();
		ProductService productService = new ProductServiceImpl(productRepository);
		ProductController productController = new ProductController(productService);

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Nhap vao category tag can tim kiem: ");
		String searchCategoryTag = sc.nextLine();

		System.out.println("\n\t\t======SORT BY PRICE======\n");
		productController.sortProductByPrice(searchCategoryTag);

		System.out.println("\n\t\t======SORT BY NAME======\n");
		productController.sortProductByName(searchCategoryTag);
	}
}
