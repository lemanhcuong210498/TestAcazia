package com.lecuong.repository;

import java.util.List;

import com.lecuong.entity.Product;

public interface ProductRepository {
	
	List<Product> findAll();
	
}
