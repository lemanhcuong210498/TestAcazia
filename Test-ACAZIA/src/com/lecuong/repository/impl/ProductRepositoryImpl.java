package com.lecuong.repository.impl;

import java.util.ArrayList;
import java.util.List;

import com.lecuong.entity.Category;
import com.lecuong.entity.Product;
import com.lecuong.repository.ProductRepository;

public class ProductRepositoryImpl implements ProductRepository {
	
	private static List<Product> listProducts = new ArrayList<Product>();

	static {

		Category category1 = new Category("may tinh", "mt");
		Category category2 = new Category("dien lanh", "dl");

		listProducts.add(new Product("may tinh xach tay A", "mt", 12.0));
		listProducts.add(new Product("laptop X", "mt", 13.0));
		listProducts.add(new Product("pc 2", "mt", 13.0));
		listProducts.add(new Product("ultrabook EZ", "mt", 16.0));
		listProducts.add(new Product("tu lanh e", "dl", 11.5));
		listProducts.add(new Product("dieu hoa nhiet do b", "dl", 12.0));
	}

	@Override
	public List<Product> findAll() {
		return listProducts;
	}
	
}
