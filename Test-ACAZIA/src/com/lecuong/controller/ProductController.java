package com.lecuong.controller;

import java.util.ArrayList;
import java.util.List;

import com.lecuong.entity.Product;
import com.lecuong.service.ProductService;
import com.lecuong.service.impl.ProductServiceImpl;

public class ProductController {
	
	private final ProductService productService;

	public ProductController(ProductService productService) {
		this.productService = productService;
	}
	
	public void sortProductByPrice(String searchCategoryTag) {
		productService.sortProductByPrice(searchCategoryTag);
	}
	
	public void sortProductByName(String searchCategoryTag) {
		productService.sortProductByName(searchCategoryTag);
	}
	
}
